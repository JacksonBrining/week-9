#include <stdio.h>
#include <string.h>

union GameData {
    int Score;
    float Threes;
    char str[25];

};

int main (){
    union GameData data;

    data.Score = 25;
    data.Threes = 9;
    strcpy(data.str, "My Really Badly Played Basketball Game");

    printf("Score : %d\n", data.Score);
    printf("Amount of Three's in game : %d\n", data.Threes);
    printf("What is the Event : %s\n", data.str);

    return 0;

}
