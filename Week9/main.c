#include <stdio.h>
#include <string.h>

struct Phones {
    char make[50];
    char model[50];
    int year;
    char color[15];
};

void printPhones( struct Phones phone );

int main(){

    struct Phones myphone;
    struct Phones mysistersphone;

    strcpy(myphone.make, "Apple Iphone");
    strcpy(myphone.model, "X");
    strcpy(myphone.color, "Grey");
    myphone.year = 2017;

    strcpy(mysistersphone.make, "Samsung");
    strcpy(mysistersphone.model, "Galaxy");
    strcpy(mysistersphone.color, "Black");
    mysistersphone.year = 2017;

    printPhones( mysistersphone );
    return 0;

}

void printPhones( struct Phones Phone ) {

    printf("Add 1 : %s\n", Phone.make);
    printf("Add 2 : %s\n", Phone.model);
    printf("Add 3 : %s\n", Phone.color);
    printf("Add 4 : %s\n", Phone.year);

}


